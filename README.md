![dotfiles](dotfiles_screenshot.png "i3wm screenshot")

# Here Be Dragons

- Probably not POSIX compliant.

- [Imgur album | 20+ screenshots](https://imgur.com/a/VXpYHBM)

- I make no claims regarding my competency.

- Definitely not ideal for a multi user system.

- For CPU heavy tasks I use Enlightenment or Openbox.


